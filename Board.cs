﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pishkvorky
{
    class Board
    {
        private static Shanpe playerShanpe_m;

        public static void setPlayerShanpe(Shanpe newPlayerShanpe)
        {
            playerShanpe_m = newPlayerShanpe;
        }

        public static Shanpe getPlayerShanpe()
        {
            return playerShanpe_m;
        }

        public static Shanpe getEnemyShanpe()
        {
            if (playerShanpe_m == Shanpe.circle)
            {
                return Shanpe.cross;
            }
            else if(playerShanpe_m == Shanpe.cross)
            {
                return Shanpe.circle;
            }
            else
            {
                return Shanpe.empty;
            }
        }

        private static Shanpe[,] board_m = new Shanpe[3, 3];

        public static bool placeShanpe(Shanpe shanpe, int posX, int posY)
        {
            if (board_m[posX, posY] != Shanpe.empty)
            {
                return false;
            }

            board_m[posX, posY] = shanpe;

            return true;
        }

        public static Shanpe[,] get()
        {
            return board_m;
        }

        public static BoardSquare[] emptySquares(Shanpe[,] board = null)
        {
            Shanpe[,] tmpBoard = null;

            if (board != null)
            {
                tmpBoard = board;
            } else
            {
                tmpBoard = board_m;
            }

            List<BoardSquare> squares = new List<BoardSquare>();

            for(int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (tmpBoard[x, y] == Shanpe.empty)
                    {
                        squares.Add(new BoardSquare(x, y));
                    }
                }
            }
            return squares.ToArray();
        }

        public static void testSquares()
        {
            foreach (Shanpe shanpe in board_m)
            {
                Console.WriteLine(shanpe.ToString());
            }
        }

        public static void print(int selectedX = -1, int selectedY = -1)
        {
            /*
            board_m[1, 2] = Shanpe.circle;
            board_m[2, 1] = Shanpe.cross;
            */

            Console.Clear();

            for (int x = 0; x < 3; x++)
            {
                Console.Write(" ");
                for (int y = 0; y < 3; y++)
                {
                    switch(board_m[x, y])
                    {
                        case Shanpe.empty:
                            Console.Write("_");
                            break;
                        case Shanpe.circle:
                            Console.Write("o");
                            break;
                        case Shanpe.cross:
                            Console.Write("x");
                            break;

                    }
                    if (selectedX == x && selectedY == y)
                    {
                        Console.Write("<");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }

            //Console.WriteLine("Debug: ");
            //Console.WriteLine("AI move: " + AI.getLastMove()?.ToString());
        }
    }

    class BoardSquare
    {
        public readonly int x;
        public readonly int y;

        public BoardSquare(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    enum Shanpe {
        empty, cross, circle
    }
}
