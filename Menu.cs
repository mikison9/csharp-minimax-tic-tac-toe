﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Pishkvorky
{
    class Menu
    {
        private static bool twoPlayer = false;

        private static bool playerOneFirst = false;

        private static Random rand = new Random();

        private static String userName = "";

        private static bool isTwoPlayer()
        {
            return twoPlayer;
        }

        public static void enterUserName()
        {
            Console.Write("Enter your name: ");
            userName = Console.ReadLine();
        }

        public static void pickMode()
        {
            int pickedIndex = 0;

            ConsoleKeyInfo pressed;

            do
            {
                Console.Clear();
                Console.WriteLine("Hello! Welcome to Tic Tac Toe.");
                Console.WriteLine("Are you playing alone or with a friend?");

                if (pickedIndex == 0)
                {
                    Console.WriteLine("2p< 1p  ");
                }
                else
                {
                    Console.WriteLine("2p  1p< ");
                }

                pressed = Console.ReadKey(true);

                switch (pressed.Key)
                {
                    case ConsoleKey.LeftArrow:
                        pickedIndex = 0;
                        break;
                    case ConsoleKey.RightArrow:
                        pickedIndex = 1;
                        break;
                    case ConsoleKey.Enter:
                        if (pickedIndex == 0)
                        {
                            twoPlayer = true;
                        }
                        break;
                }
            } while (pressed.Key != ConsoleKey.Enter);
        }

        public static void pickSide()
        {
            int pickedIndex = 0;

            ConsoleKeyInfo pressed;

            do
            {
                Console.Clear();
                Console.WriteLine("Player 1, pick circles or crosses... ");

                if (pickedIndex == 0)
                {
                    Console.WriteLine("x< o  ");
                }
                else
                {
                    Console.WriteLine("x  o< ");
                }

                pressed = Console.ReadKey(true);

                switch (pressed.Key)
                {
                    case ConsoleKey.LeftArrow:
                        pickedIndex = 0;
                        break;
                    case ConsoleKey.RightArrow:
                        pickedIndex = 1;
                        break;
                    case ConsoleKey.Enter:
                        if (pickedIndex == 0)
                        {
                            Board.setPlayerShanpe(Shanpe.cross);
                        }
                        else
                        {
                            Board.setPlayerShanpe(Shanpe.circle);
                        }
                        break;
                }
            } while (pressed.Key != ConsoleKey.Enter);

            if (rand.Next(100) < 50)
            {
                playerOneFirst = true;
            }

            Console.WriteLine("{0} starts...", playerOneFirst ? "Player 1" : "Player 2 / AI");
            Console.WriteLine("Press any key to start.");
            Console.ReadKey(true);
        }

        public static void play()
        {
            while (true)
            {
                if (playerOneFirst)
                {
                    playerPlay();
                    if (checkEnd())
                    {
                        return;
                    }

                    enemyPlay();
                }
                else
                {
                    enemyPlay();
                    if (checkEnd())
                    {
                        return;
                    }

                    playerPlay();
                }                
            }
        }

        private static bool checkEnd()
        {
            int isEnd = AI.isEnd();

            if (isEnd != 3)
            {
                end(isEnd);
                return true;
            }
            return false;
        }

        private static void end(int status)
        {
            Console.Clear();

            Board.print();

            switch (status)
            {
                case 0:
                    Console.WriteLine("It's a draw!");
                    Score.draw(userName);
                    break;
                case 1:
                    Console.WriteLine("Player 2 / AI won!");
                    Score.lost(userName);
                    break;
                case 2:
                    Console.WriteLine("Player 1 won!");
                    Score.win(userName);
                    break;
            }

            Console.ReadKey(true);
        }

        private static void playerPlay(bool enemyTurn = false)
        {
            int pickedX = 0;
            int pickedY = 0;

            ConsoleKeyInfo pressed;
            bool placed = false;

            do
            {
                Board.print(pickedX, pickedY);

                pressed = Console.ReadKey(true);

                switch (pressed.Key)
                {
                    case ConsoleKey.LeftArrow:
                        if (pickedY != 0)
                        {
                            pickedY--;
                        }
                        break;

                    case ConsoleKey.RightArrow:
                        if (pickedY != 2)
                        {
                            pickedY++;
                        }
                        break;

                    case ConsoleKey.UpArrow:
                        if (pickedX != 0)
                        {
                            pickedX--;
                        }
                        break;

                    case ConsoleKey.DownArrow:
                        if (pickedX != 2)
                        {
                            pickedX++;
                        }
                        break;

                    case ConsoleKey.Enter:
                        if (Board.placeShanpe(enemyTurn ? Board.getEnemyShanpe() : Board.getPlayerShanpe(), pickedX, pickedY) == true)
                        {
                            placed = true;
                        }

                        break;
                }

            } while (pressed.Key != ConsoleKey.Enter || placed == false);
        }

        private static void enemyPlay()
        {
            if (isTwoPlayer())
            {
                playerPlay(true);
            }
            else
            {
                AI.makeMove();
            }
        }
    }
}
