﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pishkvorky
{
    class Program
    {
        static void Main(string[] args)
        {
            Score.parseFile();
            Menu.enterUserName();
            Menu.pickMode();
            Menu.pickSide();
            Menu.play();
            Score.save();
            Score.printScores();
            //Console.ReadKey(true);
        }
    }
}
