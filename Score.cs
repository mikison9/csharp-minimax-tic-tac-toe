﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pishkvorky
{
    class Score
    {
        static String usersFile = "users.txt";
        static List<User> users;

        public static void parseFile()
        {
            users = new List<User>();

            if (File.Exists(usersFile))
            {
                StreamReader file = new StreamReader(usersFile);

                String line;

                while (true)
                {
                    line = file.ReadLine();

                    if (line != null)
                    {
                        String[] fields = line.Split(' ');

                        int wins;
                        int losses;
                        int draws;

                        int.TryParse(fields[1], out wins);
                        int.TryParse(fields[2], out losses);
                        int.TryParse(fields[3], out draws);


                        User us = new User(fields[0], wins, losses, draws);

                        users.Add(us);
                    }
                    else
                    {
                        break;
                    }

                } 

                file.Close();
            }
        }

        public static void win(String userName)
        {
            int userInd = addIfNExists(userName);

            users.ElementAt(userInd).won();
        }

        public static void lost(String userName)
        {
            int userInd = addIfNExists(userName);

            users.ElementAt(userInd).lost();
        }

        public static void draw(String userName)
        {
            int userInd = addIfNExists(userName);

            users.ElementAt(userInd).draw();
        }

        public static void save()
        {
            StreamWriter file = new StreamWriter(usersFile);
            //lol

            foreach (User us in users.ToArray())
            {
                file.WriteLine("{0} {1} {2} {3}", us.getName(), us.getWins(), us.getLosses(), us.getDraws());
            }

            file.Close();
        }

        static int addIfNExists(String userName)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users.ElementAt(i).getName() == userName)
                {
                    return i;
                }
            }

            User us = new User(userName, 0, 0, 0);

            users.Add(us);

            return users.Count - 1;
        }

        public static void printScores()
        {
            Console.Clear();
            Console.WriteLine("Scores: ");

            foreach (User u in users)
            {
                Console.WriteLine("Name: {0} Wins: {1} Losses: {2} Draws: {3}", u.getName(), u.getWins(), u.getLosses(), u.getDraws());
            }

            Console.ReadKey(true);
        }
    }

    class User
    {
        String name;
        int wins;
        int losses;
        int draws;

        public String getName()
        {
            return name;
        }
    
        public int getWins()
        {
            return wins;
        }

        public int getLosses()
        {
            return losses;
        }

        public int getDraws()
        {
            return draws;
        }

        public void won()
        {
            wins++;
        }

        public void lost()
        {
            losses++;
        }

        public void draw()
        {
            draws++;
        }

        public User(String name, int wins, int losses, int draws)
        {
            this.name = name;
            this.wins = wins;
            this.losses = losses;
            this.draws = draws;
        }
    }
}
