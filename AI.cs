﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pishkvorky
{
    class AI
    {
        private static MinimaxMove lastMove;

        public static MinimaxMove getLastMove()
        {
            return lastMove;
        }

        static public void makeMove()
        {
            MinimaxMove move = minimax();
            Board.placeShanpe(Board.getEnemyShanpe(), move.x, move.y);
            lastMove = move;
        }

        //0 - draw
        //1 - player 2 / ai won
        //2 - player 1 won
        //3 - no
        static public int isEnd()
        {
            int check = checkWin(Board.get());

            if (Board.emptySquares().Length == 0 && check == 0)
            {
                return 0;
            }

            return check == 0 ? 3 : check;
        }

        static private MinimaxMove minimax(int moveCount = 0, Shanpe[,] board = null, int moveX = 0, int moveY = 0, bool me = true)
        {
            moveCount++;

            if (board == null)
            {
                board = Board.get();
            }

            BoardSquare[] empty = Board.emptySquares(board);

            int win = checkWin(board);

            if (win == 1)
            {
                return new MinimaxMove(10, moveX, moveY, moveCount);
            }
            else if (win == 2)
            {
                return new MinimaxMove(-10, moveX, moveY, moveCount);
            }
            else if (empty.Length == 0)
            {
                return new MinimaxMove(0, moveX, moveY, moveCount);
            }

            MinimaxMove[] moves = new MinimaxMove[empty.Length];

            for (int i = 0; i < empty.Length; i++)
            {
                BoardSquare square = empty[i];
                Shanpe[,] newBoard = new Shanpe[3, 3];
                Array.Copy(board, newBoard, 3*3);
                //board.CopyTo(newBoard, 0);
                newBoard[square.x, square.y] = me ? Board.getEnemyShanpe() : Board.getPlayerShanpe();
                
                moves[i] = minimax(moveCount, newBoard, square.x, square.y, !me);
            }

            MinimaxMove bestMove = null;

            if (!me)
            {
                int highestScore = -999999999;
                for (int i = 0; i < moves.Length; i++)
                {

                    if (moves[i].score > highestScore)
                    {
                        highestScore = moves[i].score;
                        bestMove = moves[i];
                    }
                }
            }
            else
            {
                int lowestScore = 999999999;
                for (int i = 0; i < moves.Length; i++)
                {

                    if (moves[i].score < lowestScore)
                    {
                        lowestScore = moves[i].score;
                        bestMove = moves[i];
                    }
                }
            }

            return bestMove;
        }


        //0 = nikdo
        //1 = ja (pocitac)
        //2 = protivnik (hrac)
        static private int checkWin(Shanpe[,] board)
        {
            Shanpe AIShanpe = Board.getEnemyShanpe();
            Shanpe playerShanpe = Board.getPlayerShanpe();

            if (checkWinShanpe(board, AIShanpe) == true)
            {
                return 1;
            }
            else if (checkWinShanpe(board, playerShanpe) == true)
            {
                return 2;
            }

            return 0;
        }

        static private bool checkWinShanpe(Shanpe[,] board, Shanpe shanpe)
        {
            for (int x = 0; x < 3; x++)
            {
                if (board[x, 0] == shanpe && board[x, 1] == shanpe && board[x, 2] == shanpe)
                {
                    return true;
                }
            }

            for (int y = 0; y < 3; y++)
            {
                if (board[0, y] == shanpe && board[1, y] == shanpe && board[2, y] == shanpe)
                {
                    return true;
                }
            }

            if (board[0, 0] == shanpe && board[1, 1] == shanpe && board[2, 2] == shanpe)
            {
                return true;
            }
            else if (board[0, 2] == shanpe && board[1, 1] == shanpe && board[2, 0] == shanpe)
            {
                return true;
            }

            return false;
        }
    }

    class MinimaxMove
    {
        public readonly int score;
        public readonly int x;
        public readonly int y;
        public readonly int moveCount;

        public MinimaxMove(int score, int x, int y, int moveCount)
        {
            this.score = score;
            this.x = x;
            this.y = y;
            this.moveCount = moveCount;
        }

        public override string ToString()
        {
            return String.Format("score: {0}\nx: {1}\ny: {2}\nmoveCount: {3}\n", score, x, y, moveCount);
        }
    }
}
